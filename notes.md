### Testing workshop


## Presentation

## JS tests
 - Client vs Node vs PhantomJS
 - organising your code
 - AMD
 - organising tests
 - mocks and spies

## PHP tests
 - PHPUnit
 - TDD
 - Coverage, how is it affected by TDD
 - Refactor until it becomes a DSL
 - Does it make sense to use Behat at this point?