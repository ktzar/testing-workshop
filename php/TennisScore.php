<?php

class TennisScore {

    private $_a, $_b;
    private $_aName = "";
    private $_bName = "";

    public function __construct($aName, $bName) {
        $this->_aName = $aName;
        $this->_bName = $bName;

        $this->_a = $this->_b = [
            'game' => 0,
            'points' => 0,
        ];
    }

    public function printScore() {
        $gutter = "  ";
        $namesLength = max(strlen($this->_aName), strlen($this->_bName));
        $game = "GAMES";
        $points = "POINTS";

        echo str_pad(strtoupper($this->_aName), $namesLength, " ", STR_PAD_RIGHT);
        echo $gutter . str_pad($this->_a['game'], strlen($game), " ", STR_PAD_LEFT);
        echo $gutter . str_pad($this->_a['points'], strlen($points), " ", STR_PAD_LEFT);
        echo "\n";
        echo str_pad("", $namesLength) . $gutter . $game . $gutter . $points;
        echo "\n";
        echo str_pad(strtoupper($this->_bName), $namesLength, " ", STR_PAD_RIGHT);
        echo $gutter . str_pad($this->_b['game'], strlen($game), " ", STR_PAD_LEFT);
        echo $gutter . str_pad($this->_b['points'], strlen($points), " ", STR_PAD_LEFT);
        echo "\n";
    }

    public function getScore() {
        return [
            [$this->_a['game'], $this->_a['points']],
            [$this->_b['game'], $this->_b['points']],
        ];
    }

    public function scoreA() {
        $this->_scorePlayer($this->_a);
    }

    public function scoreB() {
        $this->_scorePlayer($this->_b);
    }

    private function _scorePlayer(&$player) {
        $player['points'] += 15;
        if ($player['points'] === 45) {
            $player['points'] = 40;
        }
        if ($player['points'] > 40) {
            $player['points'] = 0;
            $player['game'] ++;
        }
    }
}
