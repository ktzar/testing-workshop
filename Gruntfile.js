module.exports = function(grunt) {
    grunt.initConfig({
        meta: {
            package: grunt.file.readJSON('package.json'),
            src: {
                main: 'static/js',
                test: 'static/js/specs'
            },
            bin: {
                coverage: 'bin/coverage'
            }
        },
        jasmine: {
            dev: {
                src: '<%= meta.src.main %>/**/*.js',
                options: {
                    specs: '<%= meta.src.test %>/*.js',
                    template: require('grunt-template-jasmine-requirejs'),
                    templateOptions: {
                        requireConfig: {
                            baseUrl: '<%= meta.src.main %>/src',
                            paths: {
                                jquery: '../lib/jquery-2.1.4.min'
                            }
                        }
                    }
                }
            },
            coverage: {
                src: '<%= meta.src.main %>/**/*.js',
                options: {
                    specs: '<%= meta.src.test %>/*.js',
                    template: require('grunt-template-jasmine-istanbul'),
                    templateOptions: {
                        coverage: '<%= meta.bin.coverage %>/coverage.json',
                        report: [{
                            type: 'html',
                            options: {
                                dir: '<%= meta.bin.coverage %>/html'
                            }
                        }, {
                            type: 'text-summary'
                        }],
                        template: require('grunt-template-jasmine-requirejs'),
                        templateOptions: {
                            requireConfig: {
                                baseUrl: '.grunt/grunt-contrib-jasmine/<%= meta.src.main %>/src',
                                paths: {
                                    jquery: '../lib/jquery-2.1.4.min'
                                }
                            }
                        }
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jasmine');

    grunt.registerTask('test:coverage', ['jasmine:coverage']);
};
