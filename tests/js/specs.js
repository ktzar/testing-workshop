var specs = [
    '../specs/carousel'
];

requirejs.config({
  baseUrl: '/static/js/src',
  paths: {
      jquery: 'lib/jquery-2.1.4.min'
  }
});

require(specs, function () {
  window.onload();
});
