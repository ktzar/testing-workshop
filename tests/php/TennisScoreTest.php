<?php
include("php/TennisScore.php");

class Runner_BaseTestRunnerTest extends PHPUnit_Framework_TestCase {

    private $_game;

    public function setUp() {
        $this->_game = new TennisScore("Miguel", "James");
    }

    public function testInitialScore() {
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 0], [0, 0]], $score);
    }

    public function testAScores() {
        $this->_game->scoreA();
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 15], [0, 0]], $score);
    }

    public function testBScores() {
        $this->_game->scoreB();
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 0], [0, 15]], $score);
    }

    public function testMakeGame() {
        $this->_scoreTimes('b', 4);
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 0], [1, 0]], $score);
    }

    public function testAllScores() {
        $this->_scoreTimes('a', 2);
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 30], [0, 0]], $score);
        $this->_scoreTimes('a', 1);
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 40], [0, 0]], $score);

        $this->_game = new TennisScore("A", "B");
        $this->_scoreTimes('b', 2);
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 0], [0, 30]], $score);
        $this->_scoreTimes('b', 1);
        $score = $this->_game->getScore();
        $this->assertEquals([[0, 0], [0, 40]], $score);
    }

    public function testPrintResult() {
        $this->expectOutputString(<<<EOT
MIGUEL      0      15
        GAMES  POINTS
JAMES       0       0

EOT
        );
        $this->_scoreTimes('a', 1);
        $this->_game->printScore();
    }

    private function _scoreTimes($player, $times) {
        $func = $player === "a" ? "scoreA" : "scoreB";
        for ($i = 0;$i < $times; $i ++) {
            $this->_game->$func();
        }
    }

    private function _assertPoints($player, $points) {
        $points = $this->_game->getScore();
        $index = $player === "a" ? 0 : 1;
        $this->assertEquals([[0, 0], [0, 0]], $score);
    }

}
