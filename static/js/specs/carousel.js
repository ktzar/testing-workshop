define(['jquery', 'modules/carousel'], function ($, Carousel) {

    describe('Carousel', function () {
        var markup, carousel;

        beforeEach(function () {
            markup = $('<div></div>');
            carousel = new Carousel(markup); 
        });

        function getSlidesText() {
            return markup.find('.carousel__slides').text();
        }

        it('can add basic markup on initialising', function () {
            expect(markup.find('.carousel').length).toBe(1);
            expect(markup.find('.carousel__slides').length).toBe(1);
        });

        it('can add control buttons', function () {
            expect(markup.find('.carousel__btn').length).toBe(2);
        });

        it('can wire up buttons with actions', function () {
            spyOn(carousel, 'nextSlide');
            spyOn(carousel, 'previousSlide');

            markup.find('.js-left').trigger('click');
            expect(carousel.previousSlide).toHaveBeenCalled();

            markup.find('.js-right').trigger('click');
            expect(carousel.nextSlide).toHaveBeenCalled();
        });

        it('can loop', function () {
            carousel.addSlide('one');
            carousel.addSlide('two');
            carousel.addSlide('three');

            expect(carousel.activeSlide).toBe(0);
            carousel.nextSlide();
            expect(carousel.activeSlide).toBe(1);
            carousel.nextSlide();
            expect(carousel.activeSlide).toBe(2);
            carousel.nextSlide();
            expect(carousel.activeSlide).toBe(0);

            carousel.previousSlide();
            expect(carousel.activeSlide).toBe(2);
            carousel.previousSlide();
            expect(carousel.activeSlide).toBe(1);
            carousel.previousSlide();
            expect(carousel.activeSlide).toBe(0);
            carousel.previousSlide();
            expect(carousel.activeSlide).toBe(2);
        });

        it('can add a slide', function () {
            carousel.addSlide('some content');
            expect(markup.find('.carousel__slide').length).toBe(1);
            expect(getSlidesText()).toBe('some content');
        });

        it('can remove a slide', function () {
            carousel.addSlide('some content');
            carousel.addSlide('and more content');
            expect(markup.find('.carousel__slide').length).toBe(2);

            carousel.removeSlide(0);
            expect(markup.find('.carousel__slide').length).toBe(1);
            expect(getSlidesText()).toBe('and more content');
        });

        it('can animate to a slide', function (done) {
            carousel.animateTo(4).done(function () {
                expect(carousel.activeSlide).toBe(4);
                done();
            });
        });

        it('can load a slide content from a url', function (done) {
            spyOn($, 'get').and.returnValue($.Deferred().resolve('testing'));
            carousel.loadSlideFromUrl('testUrl').done(function () {
                expect(getSlidesText()).toBe('testing');
                done();
            });
        });
    });
});
