define(['jquery'], function ($) {
    return function (container) {
        var that = this;

        this.totalSlides = 0;
        this.activeSlide = 0;

        this.cache = {
            carousel: $(
          '<div class="carousel">'+
              '<div class="carousel__slides"></div>'+
              '<button class="js-left carousel__btn">Left</button>'+
              '<button class="js-right carousel__btn">Right</button>'+
          '</div>')
        };
        container.append(this.cache.carousel);

        container.find('.js-left').on('click', function () {
            that.previousSlide();
        });

        container.find('.js-right').on('click', function () {
            that.nextSlide();
        });

        this._addSlideContent = function (content) {
            this.cache.carousel
                .find('.carousel__slides')
                .append('<div class="carousel__slide">' + content + '</div>'); 
        };

        this._makeSlideActive = function (index) {
            this.activeSlide = index;
            this.cache.carousel
                .find('.carousel__slide')
                .removeClass('active')
                .eq(index)
                .addClass('active');
        };

        this.loadSlideFromUrl = function (url) {
            var defer = $.Deferred();
            $.get(url).done(function (data) {
                that._addSlideContent(data);
                defer.resolve();
            });
            return defer;
        };

        this.addSlide = function (text) {
            this._addSlideContent(text);
            this.totalSlides++;
        };

        this.removeSlide = function (index) {
            this.cache.carousel
                .find('.carousel__slide')
                .eq(index)
                .remove();
        };

        this.animateTo = function (newSlide) {
            var defer = $.Deferred();
            setTimeout(function () {
                that._makeSlideActive(newSlide);
                defer.resolve();
            }, 1500);
            return defer;
        };

        this.nextSlide = function () {
            if (this.activeSlide < this.totalSlides - 1) {
                this._makeSlideActive(this.activeSlide+1);
            } else {
                this._makeSlideActive(0);
            }
        };

        this.previousSlide = function () {
            if (this.activeSlide > 0) {
                this._makeSlideActive(this.activeSlide-1);
            } else {
                this._makeSlideActive(this.totalSlides - 1);
            }
        };
    };
});
