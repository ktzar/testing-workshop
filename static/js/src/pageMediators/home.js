define([
    'jquery',
    'modules/carousel'
], function($, Carousel) {
    return {
        init: function () {
            var carousel = new Carousel($('#content'));
            carousel.addSlide('hello');
            carousel.addSlide('world');
        }
    };
});
